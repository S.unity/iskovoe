const sendMessHead = document.getElementById('sendMessHead')
const sendMessFoot = document.getElementById('sendMessFoot')

async function send (nameId, emailId, phoneId, siteName, btn) {
  // отправка на срм
  const name = document.getElementById(nameId).value
  const email = document.getElementById(emailId).value
  const phone = document.getElementById(phoneId).value
  let nameTheme = null
  if (btn === sendMessHead) {
    nameTheme = 'БЕСПЛАТНАЯ КОНСУЛЬТАЦИЯ'
  } else if (btn === sendMessFoot) {
    nameTheme = 'УЗНАЙТЕ КАК'
  }
  if (
    name.length &&
    phone.length &&
    email.length
  ) {
    btn.innerText = 'Подождите...'
    const formData = new FormData()
    formData.append('crm', '16')
    formData.append('pipe', '34')
    formData.append('contact[name]', name)
    formData.append('contact[466]', phone)
    formData.append('contact[467]', email)
    formData.append('lead[541]', siteName)
    formData.append('note', 'Заявка с ' + siteName)
    await axios.post('https://bez.v-avtoservice.com/api/import-lead',
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }
    ).then(res => {
      console.log(res.data)
      if (res.data.text !== 'Все данные успешно импортированы') {
        console.log(res.data.link.result.error_message)
        alert('ошибка, попробуйте позже')
      } else {
        // document.getElementById('modalSubscribe').style.display = 'block'
        btn.innerText = 'Заявка отправлена!'
        btn.style.cssText = 'pointer-events: none'
      }
    })
  } else {
    btn.innerText = 'Заполните все поля!'
    setTimeout(() => {
      btn.innerText = 'Отправить'
    }, 2500)
  }
}

// в зависимости от того, на какую кнопку нажали, применяются разные аргументы для функции sendMess()
sendMessHead.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessHead === e.target) {
    send('head-name', 'head-email', 'head-phone', 'иск.мфюц.рф', sendMessHead)
  }
})
sendMessFoot.addEventListener('click', (e) => {
  e.preventDefault()
  if (sendMessFoot === e.target) {
    send('foot-name', 'foot-email', 'foot-phone', 'иск.мфюц.рф', sendMessFoot)
  }
})

// добавления класса по событию скролла
let el = document.querySelector('.header');

window.addEventListener('scroll', toggleClassOnScroll.bind(el, 15));

function toggleClassOnScroll(pxAmount) {
  let scrollTop = document.body.scrollTop;
  console.log(document.body.scrollTop)

  if(scrollTop > pxAmount) {
    this.classList.add('scroll--active');
  } else {
    this.classList.remove('scroll--active');
  }
}

//Анимация плавного появления блоков
const animItems = document.querySelectorAll('.anim-items');
if (animItems.length > 0) {
  window.addEventListener('scroll', animOnScroll);

  function animOnScroll(){
    for (let i = 0; i < animItems.length; i++) {
      const animItem = animItems[i];
      const animItemHeight = animItem.offsetHeight;
      const animItemOffset = offset(animItem).top;
      const animStart = 10;
      //moment nachala animation
      let animItemPoint = window.innerHeight - animItemHeight / animStart; //moment nachala animation
      if (animItemHeight > window.innerHeight) {
        animItemPoint = window.innerHeight - animItemHeight / animStart;
      };
      //moment polucheniya class
      if ((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)) {
        animItem.classList.add('anim');
      } else {
        if (!animItem.classList.contains('anim-no-hide')) {
          animItem.classList.remove('anim');
        };
      };
    };
  }; // Анимация плавного появления блоков


  function offset(el){
    const rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  };
  setTimeout( () => {
    animOnScroll();
  }, 300);
};

// закрытие попап "месенджеры"
// function closedPopUp () {
//   document.getElementById('modalSubscribe').style.display = 'none'
// }
